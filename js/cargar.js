$(document).ready(function() {

    // owl carousel
    var owl = $('.owl-carousel');

    if (typeof jQuery.fn.owlCarousel == 'function') {

        owl.owlCarousel({

            items: 1, //10 items above 1000px browser width
            lazyLoad: true,
            itemsDesktop: [1024, 1], //5 items between 1000px and 901px
            itemsDesktopSmall: [780, 1], // 3 items betweem 900px and 601px
            itemsTablet: [525, 1], //2 items between 600 and 0;
            itemsMobile: true, // itemsMobile disabled - inherit from itemsTablet option
            pagination: false

        });

    }

    // Custom Navigation Events
    $(".next").click(function () {
        owl.trigger('owl.next');
    });

    $(".prev").click(function () {
        owl.trigger('owl.prev');

    });

    // scroll to translate
    $(".link_traslate").on("click", function(evt){
        scroll_traslate=1;
        evt.preventDefault();
        traslate_section($(this).attr("href"));
    });

    $('#scrollUp').click(function(){

        $("html, body").animate({ scrollTop: 0 }, 1100);

        return false;

    });     

    /*-------------------------------------------------------------------------------------------------------------*/

    // Validar el formulario y enviar al correo del destinatario
    $('#enviar').click(function() {
        
        // Envio los datos de todos los campos del html
        var name        = $('input[name=nombres]');
        var email       = $('input[name=email]');       
        var phone       = $('input[name=celular]');
        var regx        = /^([a-z0-9_\-\.])+\@([a-z0-9_\-\.])+\.([a-z]{2,4})$/i;
        var comment     = $('textarea[name=mensaje]');      
        var returnError = false;
        
        // Simple validacion para saber si el usuario ingreso algun valor
        // Agrego un control de errores con js, pero tambien procesando con un archivo PHP.
        // Si encuentra el error, se agrega y resalta la clase .error a los campos de texto y al textarea.
        if (name.val()=='') 
        {
            name.addClass('error');
            returnError = true;
        } else name.removeClass('error');                   
        
        if (email.val()=='') 
        {
            email.addClass('error');
            returnError = true;
        } else email.removeClass('error');      
        
        if(!regx.test(email.val()))
        {
          email.addClass('error');
          returnError = true;
        } else email.removeClass('error');  

        if (phone.val()=='') 
        {
            phone.addClass('error');
            returnError = true;
        } else phone.removeClass('error');
        
        // A continuacion se resalta todos los campos que contengan errores.
        if(returnError == true)
        {
            return false;   
        }
        
        // Se inicia el ajax
        $.ajax({
            // Colocamos el archivo enviar.php para que realize el proceso de envio.
            url: 'enviar.php',  
            
            // el metodo que se usara es POST
            type: "POST",

            // colocamos la variable data para enviar los datos del formulario.     
            data: $('#fcontacto').serialize(),
            
            // No almacenar los temporales en la pagina
            cache: false,
            
            //success
            success: function(data){            
                
                $('#estado').fadeOut("fast",function()
                {
                    $('#estado').html(data);
                });
                
                $('#estado').fadeIn("slow");
                $("#fcontacto").find('input[type=text], textarea').val("");

            }

        });
        
        return false;
    });

});

// declaro la funcion traslate_section()

function traslate_section(href)
{
    $('html, body').animate({
        scrollTop: $(href).offset().top
    }, 1000,function(){
        scroll_traslate=0;
    });

}